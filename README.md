# Assessment -2 
FrontEnd:
==========
1. Framework used => ReactJs and Ant Design
2. Run npm install.
3. Default Login Credenials:
	User Name => demo
	Password  => demo
4. Idle Timeout Configured in env file (60 seconds)
5. Actions Implemented:
	1. Login Page
	2. Reset Password
	3. Idle Timeout

Backend API:
===========
Using .Net Core
- Technology Used:
	1. Entity Framework
	2. Sql Server
    3. MVC and REST API
