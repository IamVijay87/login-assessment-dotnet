﻿using LoginDemo.Modal.Request;
using LoginDemo.Service.Implementations;
using LoginDemo.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LoginDemo.Controllers
{
    [ApiController]
    [Route("auth")]
    public class AuthController : Controller
    {
        ILoginService loginService = new LoginService();
        
        [HttpPost]
        [Route("login")]
        public ResponseBase Login(UserRequest request)
        {
            try
            {
                string sessionId = HttpContext.Session.Id;
                ResponseBase responseBase = loginService.login(request,sessionId);
                return responseBase;
            }
            catch (Exception ex)
            {
                return ResponseUtility.Error(ex);
            }
        }

        [HttpPut]
        [Route("resetpassword")]
        public ResponseBase ResetPassword(ResetPasswordRequest request)
        {
            try
            {
                ResponseBase responseBase = loginService.resetPassword(request);
                return responseBase;
            }
            catch (Exception ex)
            {
                return ResponseUtility.Error(ex);
            }
        }

        [HttpDelete]
        [Route("destroysession")]
        public ResponseBase DestroySession(UserRequest request)
        {
            try
            {
                ResponseBase responseBase = loginService.destroySession(request.id);
                return responseBase;
            }
            catch (Exception ex)
            {
                return ResponseUtility.Error(ex);
            }
        }
    }
}
