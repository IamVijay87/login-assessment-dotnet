﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginDemo.Modal.dto
{
    public class UserDto
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string sessionId { get; set; }
    }
}
