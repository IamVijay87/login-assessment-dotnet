﻿using LoginDemo.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginDemo.Modal.Request
{
    public enum ResponseStatusCode
    {
        OK = 200,
        BAD_REQUEST = 400,
        UNAUTHORIZED = 401,
        FORBIDDEN = 403,
        NOT_FOUND = 404,
        INTERNAL_SERVER_ERROR = 500,
    }

    public static class ResponseUtility
    {
        public const string Msg_Bad_Request = "Bad Request";
        public const string Msg_No_Content = "No Content";

        public static ResponseBase Success(Object data)
        {
            return Success(ResponseStatusCode.OK, data);
        }

        public static ResponseBase Success(ResponseStatusCode statusCode, Object data)
        {
            ResponseBase responseBase = new ResponseBase();
            responseBase.StatusCode = statusCode;
            responseBase.Message = "Success";
            responseBase.Data = data;
            return responseBase;

        }

        public static ResponseBase Fail(string message)
        {
            ResponseBase responseBase = new ResponseBase();
            responseBase.StatusCode = ResponseStatusCode.BAD_REQUEST;
            responseBase.Message = message;
            responseBase.Data = null;
            return responseBase;
        }

        public static ResponseBase Fail(ResponseStatusCode statusCode, string message)
        {
            ResponseBase responseBase = new ResponseBase();
            responseBase.StatusCode = statusCode;
            responseBase.Message = message;
            responseBase.Data = null;
            return responseBase;
        }

        public static ResponseBase Error(Exception ex)
        {
            ResponseBase responseBase = new ResponseBase();
            responseBase.StatusCode = ResponseStatusCode.INTERNAL_SERVER_ERROR;
            responseBase.Message = string.Format("{0} {1}", "INTERNAL SERVER ERROR:", ex.ToString());
            responseBase.Data = null;
            return responseBase;
        }
    }
}
