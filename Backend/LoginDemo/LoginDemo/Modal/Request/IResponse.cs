﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginDemo.Modal.Request
{
    public interface IResponse
    {
        ResponseStatusCode StatusCode { get; set; }
        string Message { get; set; }
        Object Data { get; set; }
    }
}
