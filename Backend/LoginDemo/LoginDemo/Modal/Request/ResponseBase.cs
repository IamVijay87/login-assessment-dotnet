﻿using LoginDemo.Modal.Request;
using System;

namespace LoginDemo.Controllers
{
    public class ResponseBase : IResponse
    {
        public string Message { get; set; }
        public ResponseStatusCode StatusCode { get; set; }
        public object Data { get; set; }
    }
}