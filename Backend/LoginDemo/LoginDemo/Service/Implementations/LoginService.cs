﻿using LoginDemo.Controllers;
using LoginDemo.DBContext;
using LoginDemo.Modal.dto;
using LoginDemo.Modal.Request;
using LoginDemo.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace LoginDemo.Service.Implementations
{
    public class LoginService : ILoginService
    {

        public ResponseBase login(UserRequest request, string sessionId)
        {
            if (request != null && !(string.IsNullOrEmpty(request.userName)) && !(string.IsNullOrEmpty(request.password)))
            {
                UserDto userDto;
                using (var context = new AssessmentContext())
                {
                    userDto = context.UserLogins.Where(x => x.UserName == request.userName && x.Password == request.password)
                         .Select(userlogin =>
                                          new UserDto()
                                          {
                                              id = userlogin.Id,
                                              userName = userlogin.UserName,
                                              password = userlogin.Password,
                                              sessionId = sessionId
                                          }).FirstOrDefault();
                    if (userDto != null && saveSessionId(userDto, sessionId))
                    {
                        return ResponseUtility.Success(userDto);
                    }
                    else
                    {
                        return ResponseUtility.Fail("Fail");
                    }
                }

            }
            else
            {
                return ResponseUtility.Fail("Fail");
            }
        }

        public bool saveSessionId(UserDto dto,string sessionId)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    using (var context = new AssessmentContext ())
                    {
                        if(dto != null && !(string.IsNullOrEmpty(dto.userName)) && !(string.IsNullOrEmpty(dto.password)))
                        {
                            UserSession userSession = new UserSession();
                            userSession.UserId = dto.id;
                            userSession.SessionId = sessionId;
                            context.UserSessions.Add(userSession);
                        }
                        context.SaveChanges();
                    }
                    ts.Complete();
                    return true;
                }
                catch (Exception ex)
                {
                    ts.Complete();
                    return false;
                }
            }

        }

        public ResponseBase resetPassword(ResetPasswordRequest request)
        {
            if (request != null && !(string.IsNullOrEmpty(request.userName)) && !(string.IsNullOrEmpty(request.oldPassword)) && !(string.IsNullOrEmpty(request.newPassword)))
            {
                using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        using (var context = new AssessmentContext())
                        {
                            UserLogin userLogin = context.UserLogins.Where(x => x.UserName == request.userName && x.Password == request.oldPassword && x.UserSessions.Any(y => y.SessionId == request.sessionId)).FirstOrDefault();
                            if (userLogin != null)
                            {
                                userLogin.Password = request.newPassword;
                                context.SaveChanges();
                                ts.Complete();
                                return ResponseUtility.Success(userLogin);
                            }
                            else
                            {
                                ts.Complete();
                                return ResponseUtility.Fail("Fail");
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        ts.Complete();
                        return ResponseUtility.Fail(ex.Message);
                    }

                }
            }
            else
            {
                return ResponseUtility.Fail("Fail");
            }
            
        }

        public ResponseBase destroySession(int userId)
        {
            if (userId != 0 && userId != -1)
            {
                using (TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        using (var context = new AssessmentContext())
                        {
                            List<UserSession> userSession = context.UserSessions.Where(x => x.UserId == userId).ToList();
                            if (userSession != null)
                            {
                                context.UserSessions.RemoveRange(userSession);
                                context.SaveChanges();
                                ts.Complete();
                                return ResponseUtility.Success("Successfully deleted user session");
                            }
                            else
                            {
                                ts.Complete();
                                return ResponseUtility.Fail("Fail");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Complete();
                        return ResponseUtility.Fail(ex.Message);
                    }

                }
            }
            else
            {
                return ResponseUtility.Fail("Fail");
            }

        }
    }
}
