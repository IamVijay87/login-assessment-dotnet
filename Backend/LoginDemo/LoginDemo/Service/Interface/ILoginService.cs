﻿using LoginDemo.Controllers;
using LoginDemo.Modal.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginDemo.Service.Interface
{
    public interface ILoginService
    {
        public ResponseBase login(UserRequest user,string sessionId);
        public ResponseBase resetPassword(ResetPasswordRequest request);
        public ResponseBase destroySession(int userId);
    }
}
