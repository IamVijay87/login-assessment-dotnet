﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LoginDemo.DBContext
{
    public partial class UserLogin
    {
        public UserLogin()
        {
            UserSessions = new HashSet<UserSession>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public virtual ICollection<UserSession> UserSessions { get; set; }
    }
}
