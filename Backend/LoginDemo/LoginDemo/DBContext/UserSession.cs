﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LoginDemo.DBContext
{
    public partial class UserSession
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string SessionId { get; set; }

        public virtual UserLogin User { get; set; }
    }
}
